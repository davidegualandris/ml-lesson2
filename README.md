# [ML] Lesson2

These are the Jupyter Notebooks developed to perform the [second](http://home.agh.edu.pl/~mdig/dokuwiki/doku.php?id=teaching:courses:agh:weaiiib:inf:adv-ml:2018-19_l:labs:revision) and the [third](http://home.agh.edu.pl/~mdig/dokuwiki/doku.php?id=teaching:courses:agh:weaiiib:inf:adv-ml:2018-19_l:labs:lab03) assignments.